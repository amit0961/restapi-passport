<?php

namespace App\Http\Controllers;

use App\Country;
use http\Env\Response;
use Illuminate\Http\Request;

use Validator;

class countryController extends Controller
{
    public function country(){

        return response()->json(Country::get(),200);

    }
    public function   countryById($id){
        $country = Country::find($id);
        if(is_null($country)){
            return response()->json(["message"=>"Opps.... Record Not Found !" ],404);
        }
        return response()->json($country, 200);

    }
    public function countrySave(Request $request){
        $rules = [
            'name' => 'required | min:3'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }
        $country = Country::create($request->all());
        return response()->json($country,201);

    }
    public function countryUpdate(Request $request ,$id){
        $country= Country::find($id);
        if(is_null($country)){
            return response()->json(["message"=>"Opps.... Record Not Found !" ],404);
        }
        $country->update($request->all());
        return response()->json($country, 200);

    }
    public function countryDelete($id){
        $country= Country::find($id);
        if(is_null($country)){
            return response()->json(["message"=>"Opps.... Record Not Found !" ],404);
        }
        $country->delete();
        return response()->json(null, 204);
    }


}
